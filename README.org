#+title: Page After and Before

** Purpose
   :PROPERTIES:
   :CUSTOM_ID: purpose
   :END:

Provides a 'magic word' interface to retrieve /preceeding/ and /succeeding/ pages relative to a given page title.

This extension may be used for example in a simple navigation template (showing /previous///next/ links), added on top or bottom of pages that are autocategorized in the same category by their inclusion of the navigation template. It avoids the need to manually edit the previous/next links in each one of these pages (notably when these pages need to remain protected, but new pages may be added and edited by contributors into a category, using an unpredicable title name, and when these editable pages should remain navigatable from a previous or next page which is edit-protected).

This extension may also be useful to enumerate all pages listed in a category and reporting them with a custom presentation template (in a very different way, more synthetic or more detailed, than the default presentation of categories as simple paginated lists performed by MediaWiki), using a =#while= processing loop (see [[Extension:Loops##while][#while in Extension:Loops]]). But be careful in this case, because the loop may become very long to process in MediaWiki and could generate extremely large report volumes for visitors of the page, if the category is too much populated (the default limit for the Extension:Loops is set to 100, unless it has been configured): such processing loop should include a counter test and should be generating a visible warning at end of the report indicating that it is not complete and that the enumerated category should be better subcategorized to contain no more than 100 entries (or the maximum configured in that Extension:Loops).

** Features
   :PROPERTIES:
   :CUSTOM_ID: features
   :END:

- ={{= =#pagebefore:= /context/ =|= /namespace/ =|= /title/ =|= /category/ =}}=
- ={{= =#pageafter:= /context/ =|= /namespace/ =|= /title/ =|= /category/ =}}=
- ={{= =#firstpage:= /context/ =|= /namespace/ =|= /category/ =|= /filtercurrent/ =}}=
- ={{= =#lastpage:= /context/ =|= /namespace/ =|= /category/ =|= /filtercurrent/ =}}=

where all parameters are optional:

- /context/ :: is reserved for future use (should be left blank for now or unspecified)

- /namespace/ :: denotes the canonical name of the namespace (without its trailing colon) one wishes to act on (if not set, pages in the same namespace as the currently viewed page will be returned; if set and specified as blank, pages in the main namespace will be returned)

- /title/ :: denotes the 'prefixedDBkey' of a page to query about, i.e. its title name with underscores (if not set, the title of the page currently viewed will be used by default)

- /category/ :: denotes the category name used for selecting only pages that are indexed in that category (if not set, any page indexed on MediaWiki will be returned)

-  /filtercurrent/ :: allows filtering or not the returned last/first page title, when it maches the current page title (value is /yes/ or /no/, the default is /yes/):

If /filtercurrent/ is set to /yes/ (or /y/ or /1/) and the the page where the parser function is used also happens to be the last AND/OR first of the set, then it will be filtered OUT (not returned) --- this value of /yes/ is only useful if the page using the parser function is correctly sorted with an appropriate key, making it appear the first one or the last one in the specified category);

If one wishes to have for return value the title in question anyways, then one must set /filtercurrent/ to /no/ (or any value which different from /yes/, /y/ or /1/) --- note that the page using the parser function may not be the first of last page in the category, sue to its sort key or unpredictable position in the category indicated, in that case specify /no/ when calling #firstpage or #lastpage, and test the page names returned by #pagebefore or #pageafter).

** Caveats or limitations
   :PROPERTIES:
   :CUSTOM_ID: caveats_or_limitations
   :END:

Note that all these parameters may be passed by name (specified explicitly with an equal sign separator before their value), in any order, so the leading and trailing blanks in their value are ignored in that case.

The parameters can also be passed positionally, in the order specified above but whitespace stripping will only occur for the leading value of the first parameter, and for the trailing space of the last parameter (currently the code does not strip these leading an trailing whitespaces in positional parameters, and a blank but non empty parameter (except the 1st one and last one specified) will behave differently.

For this reason, the syntax form using explicitly named (or explicitly numbered) parameters is highly recommanded! It is also the only form for which is is possible to NOT set a parameter and make the distinction with an empty parameter (notably for the namespace and title parameters where this makes a difference : an unspecified namespace with an unspecified title means returning pages in any namespaces, as found in the category index (and ordered by its sort keys), or in global index of all pages in the wiki; as this global index is NOT ordered by category sort keys, the order should be the native binary order of pages in the global index of all pages of the wiki (i.e. as with the page prefix index).

The implementation is currently inconsistant with category sort keys, as it uses a mixes of ORDER BY clause (with an unnecessary LIMIT 2), based on the page title only, ignoring the namespace for unicity of these titles, and no HAVING aggregate (so it is unnecessarily slow), and the additional SQL function STRCMP to compare titles only (ignoring namespace differences and the correct ordering specified in pages added into the specified category)

This extension is still experimental and subject to changes to fix these caveats, and to handle other special cases:

- notably when these functions are used to generate textual links (it   should take care of pages found in the File or Category namespaces, by   inserting itself a leading colon for all pages not found in the   anonymous main namespace).
- or to avoid having to test the returned link in the wiki code to   generate a valid chain forming a loop in a series of pages.
