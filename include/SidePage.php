<?php

/**
 * Copyright (C) 2021  NicheWork, LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace MediaWiki\Extension\PageAfterAndBefore;

use Collation;

class SidePage extends Pager {
	public function execute() {
		$category = $this->getCategory();
		$namespace = $this->getNamespace();
		$title = $this->getTitle();
		$sortKey = Collation::singleton()->getSortKey( $title->getCategorySortKey() );
		$titles = $category->getMembers( 1, $sortKey, $namespace );

		if ( count( $titles ) ) {
			$this->value = $titles->current();
		}
		$this->final = true;
	}
}
