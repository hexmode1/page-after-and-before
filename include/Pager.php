<?php

/**
 * Copyright (C) 2021  NicheWork, LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace MediaWiki\Extension\PageAfterAndBefore;

use Collation;
use MalformedTitleException;
use MediaWiki\MediaWikiServices;
use MWNamespace;
use NicheWork\MW\ParserFunction;
use Title;

abstract class Pager extends ParserFunction {
	protected $reversed = false;

	/**
	 * Get the namespace we are resticted to
	 */
	protected function getNamespace(): int {
		$ns = $this->args['namespace'] ?? $this->parser->getTitle()->getNamespace();
		if ( is_string( $ns ) ) {
			$ns = MWNamespace::getCanonicalIndex( strtolower( $ns ) );
		}
		return $ns;
	}

	/**
	 * Get the category we're using
	 */
	protected function getCategory(): Category {
		$category = $this->args['category'] ?? null;

		if ( $category === null ) {
			throw new ParserFunctionException( "No category given" );
		}

		$catPager = Category::newFromName( $category );
		if ( $this->reversed ) {
			$catPager->setReversed();
		}

		return $catPager;
	}

	/**
	 * Get the title we're dealing with
	 */
	protected function getTitle(): Title {
		$title = $this->args['title'] ?? $this->parser->getTitle();

		if ( is_string( $title ) ) {
			try {
				$title = Title::newFromTextThrow( $title );
			} catch( MalformedTitleException $e ) {
				throw new ParserFunctionException( $e->getMessage() );
			}
		}

		return $title;
	}

	/**
	 * The page we're at
	 */
	public function getValue() {
		return $this->value;
	}
}
