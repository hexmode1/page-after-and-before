!! Version 2
# Force the test runner to ensure the extension is loaded

!! article
File:0 - At beginning, but ignore me
!! text
[[Category:Extension manual]]
!! endarticle

!! article
Extension manual
!! text
[[Category:Extension manual]]
!! endarticle

!! article
Extension manual/1 - Writing a parser function
!! text
[[Category:Extension manual]]
!! endarticle

!! article
Extension manual/2 - Writing a tag function
!! text
[[Category:Extension manual]]
!! endarticle


!! test
first page tag
!! wikitext
{{#firstpage:category=Extension manual}}
!! html
<p>Extension manual
</p>
!! end

!! test
last page tag
!! wikitext
{{#lastpage:category=Extension manual}}
!! html
<p>Extension manual/2 - Writing a tag function
</p>
!! end

!! test
page before Extension manual
!! wikitext
{{#pagebefore:category=Extension manual|title=Extension manual}}
!! html
!! end

!! test
page after Extension manual
!! wikitext
{{#pageafter:category=Extension manual|title=Extension manual}}
!! html
<p>Extension manual/1 - Writing a parser function
</p>
!! end

!! test
page before 1 - Writing a parser function
!! wikitext
{{#pagebefore:category=Extension manual|title=Extension manual/1 - Writing a parser function}}
!! html
<p>Extension manual
</p>
!! end

!! test
page after 1 - Writing a parser function
!! wikitext
{{#pageafter:category=Extension manual|title=Extension manual/1 - Writing a parser function}}
!! html
<p>Extension manual/2 - Writing a tag function
</p>
!! end

!! test
page before 2 - Writing a tag function
!! wikitext
{{#pagebefore:category=Extension manual|title=Extension manual/2 - Writing a tag function}}
!! html
<p>Extension manual/1 - Writing a parser function
</p>
!! end

!! test
page after 2 - Writing a tag function
!! wikitext
{{#pageafter:category=Extension manual|title=Extension manual/2 - Writing a tag function}}
!! html
!! end
